package com.ebiz.soap.integration.process.web;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;

import com.ebiz.soap.integration.process.service.DocumentoService;
import com.ebiz.soap.integration.wsdl.DocumentWsdlService;
import com.ebiz.soap.integration.wsdl.exception.DocumentoRequestException;
import com.ebizlatin.xsd.DocumentoRequest;

public class ReceiverComprobantePago {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReceiverComprobantePago.class);

	private CountDownLatch latch = new CountDownLatch(1);

	@Autowired
	private DocumentoService documentoService;
	
	@Autowired
	private DocumentWsdlService documentWsdlService;

	public CountDownLatch getLatch() {
		return latch;
	}

	@KafkaListener(topics = "${kafka.topic.f3.input.name}")
	public void receive(String payload) {
		LOGGER.info("received payload='{}'", payload);

		//com.ebiz.soap.integration.api.ordenes.model.POUPLOADMQ
		
		DocumentoRequest documentoRequest=null;
		try {
			documentoRequest = documentWsdlService.unmarshalDocumentoRequest(payload);
			
		} catch (DocumentoRequestException e) {
			LOGGER.error("error parser document xml",e);
		}

		if (documentoRequest != null) {
			LOGGER.info(
					"Received DocumentoRequest OK [OrgId={},Ruc={},codigoAccion={},portal={},id={},userErp={},userOauth={},pwdOauth={},docXml={}]",
					documentoRequest.getOrgId(), documentoRequest.getRuc(), documentoRequest.getCodigoAccion(), documentoRequest.getPortal(), documentoRequest.getId(),
					documentoRequest.getUserErp(), documentoRequest.getUserOauth(), documentoRequest.getPwdOauth(), documentoRequest.getDocXml());
				
			documentoService.processRequestComprobantePago(documentoRequest);
		}

		latch.countDown();
	}

}
