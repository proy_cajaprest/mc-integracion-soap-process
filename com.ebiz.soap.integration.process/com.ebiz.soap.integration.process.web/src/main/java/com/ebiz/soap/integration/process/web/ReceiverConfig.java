package com.ebiz.soap.integration.process.web;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncListenableTaskExecutor;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableKafka
public class ReceiverConfig {

  /*@Value("${kafka.bootstrap-servers}")
  private String bootstrapServers;

  @Value("${kafka.client-id}")
  private String clienteId;

  @Bean
  public Map<String, Object> consumerConfigs() {
    Map<String, Object> props = new HashMap<>();
    // list of host:port pairs used for establishing the initial connections to the Kakfa cluster
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
    props.put(ConsumerConfig.GROUP_ID_CONFIG, clienteId);
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
    props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, 100);
    props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, 15000);

    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    // allows a pool of processes to divide the work of consuming and processing records

    return props;
  }

  @Bean
  public AsyncListenableTaskExecutor execC() {
    ThreadPoolTaskExecutor tpte = new ThreadPoolTaskExecutor();
    tpte.setCorePoolSize(15);
    return tpte;
  }

  @Bean
  public AsyncListenableTaskExecutor execL() {
    ThreadPoolTaskExecutor tpte = new ThreadPoolTaskExecutor();
    tpte.setCorePoolSize(15);
    return tpte;
  }

  @Bean
  public ConsumerFactory<String, String> consumerFactory() {
    return new DefaultKafkaConsumerFactory<>(consumerConfigs());
  }

  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
    ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConcurrency(20);
    factory.getContainerProperties().setPollTimeout(3000);
    factory.setConsumerFactory(consumerFactory());

    return factory;
  }*/

  @Bean
  public ReceiverOrdenCompra receiverOrdenCompra() {
    return new ReceiverOrdenCompra();
  }

  @Bean
  public ReceiverMovimiento receiverMovimiento() {
    return new ReceiverMovimiento();
  }

  @Bean
  public ReceiverComprobantePago receiverComprobantePago() {
    return new ReceiverComprobantePago();
  }
  
  @Bean
  public ReceiverFacturaAnulacionPublicacion receiverFacturaAnulacionPublicacion() {
    return new ReceiverFacturaAnulacionPublicacion();
  }

  @Bean
  public ReceiverPagoPublicacion receiverPagoPublicacion() {
    return new ReceiverPagoPublicacion();
  }

  @Bean
  public ReceiverAnularOrden receiverAnularOrden() {
    return new ReceiverAnularOrden();
  }

}