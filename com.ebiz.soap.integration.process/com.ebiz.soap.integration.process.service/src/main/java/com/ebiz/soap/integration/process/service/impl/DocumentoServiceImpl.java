package com.ebiz.soap.integration.process.service.impl;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import com.ebiz.soap.integration.api.anularordencompra.model.RequestAnularOrden;
import com.ebiz.soap.integration.api.anularordencompra.proxy.AnularOrdenProxyApi;
import com.ebiz.soap.integration.api.comprobantepago.model.RequestCreateComprobantePago;
import com.ebiz.soap.integration.api.comprobantepago.proxy.ComprobantePagoApiProxy;
import com.ebiz.soap.integration.api.movimientos.model.RequestCreateMovimiento;
import com.ebiz.soap.integration.api.movimientos.proxy.MovimientoProxyApi;
import com.ebiz.soap.integration.api.oauth.IOAuthProxy;
import com.ebiz.soap.integration.api.oauth.model.TokenResponse;
import com.ebiz.soap.integration.api.ordenes.model.RequestCreateOC;
import com.ebiz.soap.integration.api.ordenes.proxy.OrdenesApiProxy;
import com.ebiz.soap.integration.api.pagopublicacion.model.RequestPagoPublicacion;
import com.ebiz.soap.integration.api.pagopublicacion.proxy.PagoPublicacionApiProxy;
import com.ebiz.soap.integration.api.facturaanulacion.model.RequestFacturaAnulacion;
import com.ebiz.soap.integration.api.facturaanulacion.proxy.FacturaAnulacionApiProxy;
import com.ebiz.soap.integration.fac_upload.FACUPLOADMQ;
import com.ebiz.soap.integration.mov_upload.MOVUPLOADMQ;
import com.ebiz.soap.integration.po_cancel_oc.POCANCEL;
import com.ebiz.soap.integration.po_upload.POUPLOADMQ;
import com.ebiz.soap.integration.fac_acttesor.FACACTTESORMQ;
import com.ebiz.soap.integration.fac_cambio_estado.FACCAMBIOESTADO;
import com.ebiz.soap.integration.process.service.DocumentoService;
import com.ebiz.soap.integration.schemas.DocumentXsdService;
import com.ebiz.soap.integration.schemas.exception.XmlRequestException;
import com.ebizlatin.xsd.DocumentoRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class DocumentoServiceImpl implements DocumentoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DocumentoServiceImpl.class);

	@Autowired
	private DocumentXsdService documentXsdService;

	@Autowired
	private IOAuthProxy authProxy;

	@Autowired
	private OrdenesApiProxy ordenesApiProxy;

	@Autowired
	private MovimientoProxyApi movimientoProxyApi;

	@Autowired
	private ComprobantePagoApiProxy comprobantePagoApiProxy;

	@Autowired
	private PagoPublicacionApiProxy pagoPublicacionApiProxy;

	@Autowired
	private AnularOrdenProxyApi anularOrdenProxyApi;

	@Autowired
	private FacturaAnulacionApiProxy facturaAnulacionApiProxy;

	@Autowired
	private Mapper mapper;

	@Autowired
	private MappingJackson2HttpMessageConverter springMvcJacksonConverter;

	@Override
	public void processRequestOrdenCompra(DocumentoRequest documentoRequest) {

		LOGGER.info(
				"Received DocumentoRequest OK [Ruc={},codigoAccion={},portal={},id={},userErp={},userOauth={},pwdOauth={},docXml={}]",
				documentoRequest.getRuc(), documentoRequest.getCodigoAccion(), documentoRequest.getPortal(),
				documentoRequest.getId(), documentoRequest.getUserErp(), documentoRequest.getUserOauth(),
				documentoRequest.getPwdOauth(), documentoRequest.getDocXml());

		try {

			ObjectMapper objectMapper = springMvcJacksonConverter.getObjectMapper();

			TokenResponse response = authProxy.login(documentoRequest.getUserOauth(), documentoRequest.getPwdOauth());

			LOGGER.info("User {} autenticado.", documentoRequest.getUserOauth());

			POUPLOADMQ poUploadMQ = documentXsdService.unmarshalOrdenCompraRequest(documentoRequest.getDocXml());

			com.ebiz.soap.integration.api.ordenes.model.POUPLOADMQ request = mapper.map(poUploadMQ,
					com.ebiz.soap.integration.api.ordenes.model.POUPLOADMQ.class);

			RequestCreateOC createOC = new RequestCreateOC();
			createOC.setPOUPLOADMQ(request);

			try {
				LOGGER.info(objectMapper.writeValueAsString(createOC));
			} catch (JsonProcessingException e) {
				LOGGER.error("processRequest", "writeValueAsString", e);
			}

			ordenesApiProxy.save(documentoRequest.getOrgId(), createOC, response.getAccessToken());
			LOGGER.info("Received POUPLOADMQ OK [RUC={}]", documentoRequest.getRuc());

		} catch (XmlRequestException e) {
			LOGGER.error("", e);
		}

	}

	@Override
	public void processRequestMovimiento(DocumentoRequest documentoRequest) {

		LOGGER.info(
				"Received DocumentoRequest OK [Ruc={},codigoAccion={},portal={},id={},userErp={},userOauth={},pwdOauth={},docXml={}]",
				documentoRequest.getRuc(), documentoRequest.getCodigoAccion(), documentoRequest.getPortal(),
				documentoRequest.getId(), documentoRequest.getUserErp(), documentoRequest.getUserOauth(),
				documentoRequest.getPwdOauth(), documentoRequest.getDocXml());

		try {

			ObjectMapper objectMapper = springMvcJacksonConverter.getObjectMapper();

			TokenResponse response = authProxy.login(documentoRequest.getUserOauth(), documentoRequest.getPwdOauth());

			LOGGER.info("User {} autenticado.", documentoRequest.getUserOauth());

			MOVUPLOADMQ poUploadMQ = documentXsdService.unmarshalMovimientoRequest(documentoRequest.getDocXml());

			com.ebiz.soap.integration.api.movimientos.model.MOVUPLOADMQ request = mapper.map(poUploadMQ,
					com.ebiz.soap.integration.api.movimientos.model.MOVUPLOADMQ.class);

			RequestCreateMovimiento createMov = new RequestCreateMovimiento();
			createMov.setMOVUPLOADMQ(request);

			try {
				LOGGER.info(objectMapper.writeValueAsString(createMov));
			} catch (JsonProcessingException e) {
				LOGGER.error("processRequest", "writeValueAsString", e);
			}

			movimientoProxyApi.save(documentoRequest.getOrgId(), createMov, response.getAccessToken());
			LOGGER.info("Received MOVUPLOADMQ OK [Ruc={}]", poUploadMQ.getRucCliente());

		} catch (XmlRequestException e) {
			LOGGER.error("", e);
		}

	}

	@Override
	public void processRequestComprobantePago(DocumentoRequest documentoRequest) {

		LOGGER.info(
				"Received DocumentoRequest OK [Ruc={},codigoAccion={},portal={},id={},userErp={},userOauth={},pwdOauth={},docXml={}]",
				documentoRequest.getRuc(), documentoRequest.getCodigoAccion(), documentoRequest.getPortal(),
				documentoRequest.getId(), documentoRequest.getUserErp(), documentoRequest.getUserOauth(),
				documentoRequest.getPwdOauth(), documentoRequest.getDocXml());

		try {

			ObjectMapper objectMapper = springMvcJacksonConverter.getObjectMapper();

			TokenResponse response = authProxy.login(documentoRequest.getUserOauth(), documentoRequest.getPwdOauth());

			LOGGER.info("User {} autenticado.", documentoRequest.getUserOauth());

			FACUPLOADMQ poUploadMQ = documentXsdService.unmarshalComprobantePagoRequest(documentoRequest.getDocXml());

			com.ebiz.soap.integration.api.comprobantepago.model.FACUPLOADMQ request = mapper.map(poUploadMQ,
					com.ebiz.soap.integration.api.comprobantepago.model.FACUPLOADMQ.class);

			RequestCreateComprobantePago createComprobantePago = new RequestCreateComprobantePago();
			createComprobantePago.setFACUPLOADMQ(request);

			try {
				LOGGER.info(objectMapper.writeValueAsString(createComprobantePago));
			} catch (JsonProcessingException e) {
				LOGGER.error("processRequest", "writeValueAsString", e);
			}

			comprobantePagoApiProxy.save(documentoRequest.getOrgId(), createComprobantePago, response.getAccessToken());
			LOGGER.info("Received FACUPLOADMQ OK [Ruc={}]", poUploadMQ.getRucProveedor());

		} catch (XmlRequestException e) {
			LOGGER.error("", e);
		}

	}

	@Override
	public void processRequestPagoPublicacion(DocumentoRequest documentoRequest) {
		LOGGER.info(
				"Received DocumentoRequest OK [Ruc={},codigoAccion={},portal={},id={},userErp={},userOauth={},pwdOauth={},docXml={}]",
				documentoRequest.getRuc(), documentoRequest.getCodigoAccion(), documentoRequest.getPortal(),
				documentoRequest.getId(), documentoRequest.getUserErp(), documentoRequest.getUserOauth(),
				documentoRequest.getPwdOauth(), documentoRequest.getDocXml());

		try {

			ObjectMapper objectMapper = springMvcJacksonConverter.getObjectMapper();

			TokenResponse response = authProxy.login(documentoRequest.getUserOauth(), documentoRequest.getPwdOauth());

			LOGGER.info("User {} autenticado.", documentoRequest.getUserOauth());

			FACACTTESORMQ facActTesorMQ = documentXsdService
					.unmarshalPagoPublicacionRequest(documentoRequest.getDocXml());

			com.ebiz.soap.integration.api.pagopublicacion.model.FACACTTESORMQ request = mapper.map(facActTesorMQ,
					com.ebiz.soap.integration.api.pagopublicacion.model.FACACTTESORMQ.class);

			RequestPagoPublicacion requestPagoPublicacion = new RequestPagoPublicacion();
			requestPagoPublicacion.setFACACTTESORMQ(request);

			try {
				LOGGER.info(objectMapper.writeValueAsString(requestPagoPublicacion));
			} catch (JsonProcessingException e) {
				LOGGER.error("processRequest", "writeValueAsString", e);
			}

			pagoPublicacionApiProxy.save(documentoRequest.getOrgId(), requestPagoPublicacion,
					response.getAccessToken());
			LOGGER.info("Received FACACTTESORMQ OK [Ruc={}]", facActTesorMQ.getRucCliente());

		} catch (XmlRequestException e) {
			LOGGER.error("", e);
		}

	}

	@Override
	public void processRequestFacturaAnulacion(DocumentoRequest documentoRequest) {
		LOGGER.info(
				"Received DocumentoRequest OK [Ruc={},codigoAccion={},portal={},id={},userErp={},userOauth={},pwdOauth={},docXml={}]",
				documentoRequest.getRuc(), documentoRequest.getCodigoAccion(), documentoRequest.getPortal(),
				documentoRequest.getId(), documentoRequest.getUserErp(), documentoRequest.getUserOauth(),
				documentoRequest.getPwdOauth(), documentoRequest.getDocXml());

		try {

			ObjectMapper objectMapper = springMvcJacksonConverter.getObjectMapper();

			TokenResponse response = authProxy.login(documentoRequest.getUserOauth(), documentoRequest.getPwdOauth());

			LOGGER.info("User {} autenticado.", documentoRequest.getUserOauth());

			FACCAMBIOESTADO facCambioEstado = documentXsdService
					.unmarshalFacturaAnulacionRequest(documentoRequest.getDocXml());

			com.ebiz.soap.integration.api.facturaanulacion.model.FACCAMBIOESTADO request = mapper.map(facCambioEstado,
					com.ebiz.soap.integration.api.facturaanulacion.model.FACCAMBIOESTADO.class);

			RequestFacturaAnulacion requestFacturaAnulacion = new RequestFacturaAnulacion();
			requestFacturaAnulacion.setFACCAMBIOESTADO(request);

			try {
				LOGGER.info(objectMapper.writeValueAsString(requestFacturaAnulacion));
			} catch (JsonProcessingException e) {
				LOGGER.error("processRequest", "writeValueAsString", e);
			}

			facturaAnulacionApiProxy.save(documentoRequest.getOrgId(), requestFacturaAnulacion,
					response.getAccessToken());
			LOGGER.info("Received FACCAMBIOESTADO OK [Ruc={}]", facCambioEstado.getRucCliente());

		} catch (XmlRequestException e) {
			LOGGER.error("", e);
		}

	}

	@Override
	public void processRequestAnularOrden(DocumentoRequest documentoRequest) {
		LOGGER.info(
				"Received DocumentoRequest OK [Ruc={},codigoAccion={},portal={},id={},userErp={},userOauth={},pwdOauth={},docXml={}]",
				documentoRequest.getRuc(), documentoRequest.getCodigoAccion(), documentoRequest.getPortal(),
				documentoRequest.getId(), documentoRequest.getUserErp(), documentoRequest.getUserOauth(),
				documentoRequest.getPwdOauth(), documentoRequest.getDocXml());

		try {

			ObjectMapper objectMapper = springMvcJacksonConverter.getObjectMapper();

			TokenResponse response = authProxy.login(documentoRequest.getUserOauth(), documentoRequest.getPwdOauth());

			LOGGER.info("User {} autenticado.", documentoRequest.getUserOauth());

			POCANCEL poCancel = documentXsdService.unmarshalAnularOrdenRequest(documentoRequest.getDocXml());

			com.ebiz.soap.integration.api.anularordencompra.model.POCANCEL request = mapper.map(poCancel,
					com.ebiz.soap.integration.api.anularordencompra.model.POCANCEL.class);

			RequestAnularOrden requestAnularOrden = new RequestAnularOrden();
			requestAnularOrden.setPOCANCEL(request);

			try {
				LOGGER.info(objectMapper.writeValueAsString(requestAnularOrden));
			} catch (JsonProcessingException e) {
				LOGGER.error("processRequest", "writeValueAsString", e);
			}

			anularOrdenProxyApi.save(documentoRequest.getOrgId(), requestAnularOrden, response.getAccessToken());
			LOGGER.info("Received POCANCEL OK [compradorOrgID={}, compradorUsuarioID,{}]", poCancel.getCompradorOrgID(),
					poCancel.getCompradorUsuarioID());

		} catch (XmlRequestException e) {
			LOGGER.error("", e);
		}
	}

}
