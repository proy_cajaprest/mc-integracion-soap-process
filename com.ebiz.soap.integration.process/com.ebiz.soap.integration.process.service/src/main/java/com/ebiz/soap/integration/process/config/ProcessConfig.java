package com.ebiz.soap.integration.process.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.ebiz.soap.integration.api.anularordencompra.proxy.AnularOrdenProxyApi;
import com.ebiz.soap.integration.api.anularordencompra.proxy.impl.AnularOrdenProxyApiImpl;
import com.ebiz.soap.integration.api.comprobantepago.proxy.ComprobantePagoApiProxy;
import com.ebiz.soap.integration.api.comprobantepago.proxy.impl.ComprobantePagoApiProxyImpl;
import com.ebiz.soap.integration.api.movimientos.proxy.MovimientoProxyApi;
import com.ebiz.soap.integration.api.movimientos.proxy.impl.MovimientoProxyApiImpl;
import com.ebiz.soap.integration.api.oauth.IOAuthProxy;
import com.ebiz.soap.integration.api.oauth.impl.OAuthProxyImpl;
import com.ebiz.soap.integration.api.ordenes.proxy.OrdenesApiProxy;
import com.ebiz.soap.integration.api.ordenes.proxy.impl.OrdenesApiProxyImpl;
import com.ebiz.soap.integration.api.pagopublicacion.proxy.PagoPublicacionApiProxy;
import com.ebiz.soap.integration.api.pagopublicacion.proxy.impl.PagoPublicacionApiProxyImpl;
import com.ebiz.soap.integration.api.facturaanulacion.proxy.FacturaAnulacionApiProxy;
import com.ebiz.soap.integration.api.facturaanulacion.proxy.impl.FacturaAnulacionApiProxyImpl;
import com.ebiz.soap.integration.schemas.DocumentXsdService;
import com.ebiz.soap.integration.schemas.impl.DocumentXsdServiceImpl;

@Configuration
public class ProcessConfig {
	
	@Bean
	public IOAuthProxy oAuthProxy() {
		return new OAuthProxyImpl();
	}
	
	@Bean
	public DocumentXsdService documentXsdService() {
		return new DocumentXsdServiceImpl();
	}
	
	@Bean
	public OrdenesApiProxy ordenesApiProxy() {
		return new OrdenesApiProxyImpl();
	}
	
	@Bean
	public MovimientoProxyApi movimientoProxyApi() {
		return new MovimientoProxyApiImpl();
	}
	
	@Bean
	public ComprobantePagoApiProxy comprobantePagoApiProxy() {
		return new ComprobantePagoApiProxyImpl();
	}
	
	@Bean
	public PagoPublicacionApiProxy pagoPublicacionApiProxy() {
		return new PagoPublicacionApiProxyImpl();
	}

	@Bean
	public AnularOrdenProxyApi anularOrdenProxyApi() {
		return new AnularOrdenProxyApiImpl();
	}

	@Bean
	public FacturaAnulacionApiProxy facturaAnulacionApiProxy() {
		return new FacturaAnulacionApiProxyImpl();
	}

}
