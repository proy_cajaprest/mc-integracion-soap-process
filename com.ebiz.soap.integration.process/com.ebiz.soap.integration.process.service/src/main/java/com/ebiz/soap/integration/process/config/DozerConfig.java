package com.ebiz.soap.integration.process.config;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;


@Configuration
public class DozerConfig {

    @Bean(name = "org.dozer.Mapper")
    public DozerBeanMapper dozerBean() {
        List<String> mappingFiles = Arrays.asList(
                "classpath:context/mapping/dozer-global-configuration.xml",
                "classpath:context/mapping/OrdenCompraMQXml-Json.xml",
                "classpath:context/mapping/MovimientoMQXml-Json.xml",
                "classpath:context/mapping/PagoPublicacionMQXml-Json.xml",
                "classpath:context/mapping/FacturaAnulacionMQXml-Json.xml",
                "classpath:context/mapping/AnularOrdenMQXml-Json.xml"
        );

        DozerBeanMapper dozerBean = new DozerBeanMapper();
        dozerBean.setMappingFiles(mappingFiles);
        return dozerBean;
    }

}