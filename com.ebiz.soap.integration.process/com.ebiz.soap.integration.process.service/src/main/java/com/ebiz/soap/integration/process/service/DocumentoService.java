package com.ebiz.soap.integration.process.service;

import com.ebizlatin.xsd.DocumentoRequest;

/**
 * Hello world!
 *
 */
public interface DocumentoService 
{
	void processRequestOrdenCompra(DocumentoRequest documentoRequest);
	void processRequestMovimiento(DocumentoRequest documentoRequest);
	void processRequestComprobantePago(DocumentoRequest documentoRequest);
	void processRequestPagoPublicacion(DocumentoRequest documentoRequest);
	void processRequestFacturaAnulacion(DocumentoRequest documentoRequest);
	void processRequestAnularOrden(DocumentoRequest documentoRequest);
}
